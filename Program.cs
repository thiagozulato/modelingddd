using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using Newtonsoft.Json;

namespace ddd
{
    class Program
    {
        static void Main(string[] args)
        {
            var userRepo = new UserRepository();
            var workspaceRepo = new WorkspaceRepository();

            var user = new AddNewUserCommand { Name = "Thiago", ShortName = "thiagoz" };

            var userCommandHandler = new UserCommandHandler(userRepo);
            var userId = userCommandHandler.Handler(user);

            var workspace = new AddNewWorkspace { Name = "Workspace Teste", Mode = WorkspaceMode.Private, UderId = userId };
            var workspaceCommandHandler = new WorkspaceCommandHandler(userRepo, workspaceRepo);
            workspaceCommandHandler.Handler(workspace);

            Console.WriteLine(JsonConvert.SerializeObject(userRepo.GetAllUsers()));
            Console.WriteLine();
            Console.WriteLine(JsonConvert.SerializeObject(workspaceRepo.GetAllWorkspace()));
        }
    }

    #region Domain.Core
    public abstract class Entity : IEquatable<Entity>
    {
        public Guid Id { get; private set; }

        public Entity()
        {
            Id = Guid.NewGuid();
        }

        public bool Equals(Entity entity)
        {
            if (ReferenceEquals(this, entity)) return true;
            if (ReferenceEquals(null, entity)) return false;

            return Id.Equals(entity.Id);
        }

        public override bool Equals(object entity)
        {
            return Equals(entity as Entity);
        }

        public override int GetHashCode()
        {
            return (GetType().GetHashCode() * 907) + Id.GetHashCode();
        }

        public override string ToString()
        {
            return $"{GetType().Name} [Id={Id}]";
        }
    }

    public class DomainException : Exception
    {
        public DomainException(string message) : base(message)
        {

        }
    }

    // Interface de Marcacao, pois todo repository deve ser um aggregate
    public interface IAggregateRoot { }

    public interface ICommand { }

    public interface ICommandHandler<T, R> where T : ICommand
    {
        R Handler(T @command);
    }

    public interface IRepository<A> where A : IAggregateRoot
    {

    }
    #endregion

    #region User.Domain

    public interface IUserRepository : IRepository<User>
    {
        IEnumerable<User> GetAllUsers();
        User GetUserByID(Guid id);
        void AddNewUser(User user);
    }

    public interface IWorkspaceRepository : IRepository<Workspace>
    {
        IEnumerable<Workspace> GetAllWorkspace();
        Workspace GetWorkspaceById(Guid id);
        void AddNewWorkspace(Workspace workspace);
    }

    public class User : Entity, IAggregateRoot
    {
        public string Name { get; private set; }
        public string ShortName { get; private set; }
        public bool Active { get; private set; }

        public User(string name, string shortName)
        {
            Name = name;
            ShortName = shortName;
            Active = true;
        }

        public void Deactivate()
        {
            this.Active = false;
        }

        public Workspace CreateWorksapce(string name, WorkspaceMode mode)
        {
            if (!this.Active) throw new DomainException("The user is not active to add a new Workspace");

            return new Workspace(name, mode, this.Id);
        }
    }

    public class Workspace : Entity, IAggregateRoot
    {
        public string Name { get; private set; }
        public WorkspaceMode Mode { get; private set; }
        public Guid UserId { get; private set; }

        public Workspace(string name, WorkspaceMode mode, Guid userId)
        {
            Name = name;
            Mode = mode;
            UserId = userId;
        }

        public void ChangeMode(WorkspaceMode mode)
        {
            this.Mode = mode;
        }
    }

    public enum WorkspaceMode
    {
        Public,
        Private
    }

    #region Commands
    public class UserCommandHandler : ICommandHandler<AddNewUserCommand, Guid>
    {
        readonly IUserRepository _userRepo;
        public UserCommandHandler(IUserRepository userRepo)
        {
            _userRepo = userRepo;
        }

        public Guid Handler(AddNewUserCommand command)
        {
            var user = new User(command.Name, command.ShortName);
            _userRepo.AddNewUser(user);

            return user.Id;
        }
    }

    public class WorkspaceCommandHandler : ICommandHandler<AddNewWorkspace, bool>
    {
        readonly IUserRepository _userRepo;
        readonly IWorkspaceRepository _workspaceRepo;

        public WorkspaceCommandHandler(IUserRepository userRepo, IWorkspaceRepository workspaceRepo)
        {
            _userRepo = userRepo;
            _workspaceRepo = workspaceRepo;
        }

        public bool Handler(AddNewWorkspace command)
        {
            var user = _userRepo.GetUserByID(command.UderId);
            var workspace = user.CreateWorksapce(command.Name, command.Mode);

            _workspaceRepo.AddNewWorkspace(workspace);

            return true;
        }
    }

    public class AddNewUserCommand : ICommand
    {
        public string Name { get; set; }
        public string ShortName { get; set; }
    }

    public class AddNewWorkspace : ICommand
    {
        public string Name { get; set; }
        public WorkspaceMode Mode { get; set; }
        public Guid UderId { get; set; }
    }
    #endregion

    #endregion

    #region User.Infra.Data
    public class UserRepository : IUserRepository
    {
        private readonly static List<User> _users = new List<User>();

        public void AddNewUser(User user)
        {
            _users.Add(user);
        }

        public IEnumerable<User> GetAllUsers()
        {
            return _users;
        }

        public User GetUserByID(Guid id)
        {
            return _users.Where(u => u.Id == id).FirstOrDefault();
        }
    }

    public class WorkspaceRepository : IWorkspaceRepository
    {
        private readonly static List<Workspace> _workspaces = new List<Workspace>();
        public void AddNewWorkspace(Workspace workspace)
        {
            _workspaces.Add(workspace);
        }

        public IEnumerable<Workspace> GetAllWorkspace()
        {
            return _workspaces;
        }

        public Workspace GetWorkspaceById(Guid id)
        {
            return _workspaces.Where(w => w.Id == id).FirstOrDefault();
        }
    }
    #endregion
}
